import * as React from "react";
import "../css/Contact.css";
function Contact() {
  return (
    <div className="banner-text">
      <p></p> <hr />
      <h1>Ciumafai Georgiana</h1>
      <hr />
      <p>
        <FormattedMessage
          id="Contact.telefon"
          defaultMessage="Telefon: 0741399429"
        />
      </p>
      <p>
        <FormattedMessage
          id="Contact.email1"
          defaultMessage="E-mail personal: georgi_ciumafai@yahoo.com"
        />
      </p>
      <p>
        <FormattedMessage
          id="Contact.email2"
          defaultMessage="E-mail student: Georgiana.CIUMAFAI@student.utcluj.ro"
        />
      </p>
      <p>
        <FormattedMessage
          id="Contact.email3"
          defaultMessage="E-mail profesor: Radu.Razvan.Slavescu@cs.utcluj.ro"
        />
      </p>
      <div className="social-links">
        {/* LinkedIn */}
        <a
          href="https://www.linkedin.com/in/georgiana-ciumafai"
          rel="noopener noreferrer"
          target="_blank"
        >
          <i className="fa fa-linkedin-square" aria-hidden="true" />
        </a>

        {/* Github */}
        <a
          href="https://github.com/GeorgianaCiumafai/Problems"
          rel="noopener noreferrer"
          target="_blank"
        >
          <i className="fa fa-github-square" aria-hidden="true" />
        </a>

        {/* Bitbucket */}
        <a
          href="https://bitbucket.org/ciumafai_georgiana/profile/repositories"
          rel="noopener noreferrer"
          target="_blank"
        >
          <i className="fa fa-bitbucket" aria-hidden="true" />
        </a>
      </div>
    </div>
  );
}
export default Contact;
