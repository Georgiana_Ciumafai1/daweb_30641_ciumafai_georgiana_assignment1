import * as React from "react";
import "../../src/css/Header.css";

export class Header extends React.Component {
  render() {
    return (
      <div className="header-style">
        <header>
          <FormattedMessage
            id="Header.header1"
            defaultMessage="Universitatea Tehnica din Cluj-Napoca"
          />{" "}
        </header>
        <header>
          <FormattedMessage
            id="Header.header1"
            defaultMessage="Sectia Tehnologia Informatiei"
          />
        </header>
      </div>
    );
  }
}
