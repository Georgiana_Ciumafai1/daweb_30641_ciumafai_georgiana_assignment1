import * as React from "react";
import signup from "../img/download.png";
import signup2 from "../img/py.png";
import signup3 from "../img/CLIPS.png";
import "../css/Home.css";
function Home() {
  return (
    <div className="banner-text">
      <p>
        {" "}
        <FormattedMessage
          id="Home.ThemeLicence"
          defaultMessage=" Enunţul temei: Se propune implementarea şi dezvoltarea unui sistem
          expert capabil să valideze anumite manevre de conducere şi diferite
          stiluri de condus, pe baza informaţiilor ofertie de către un simulator
          rutier."
        />
      </p>
      <p>
        {" "}
        <FormattedMessage
          id="Home.Contents"
          defaultMessage=" Conţinutul lucrării: Pagina de prezentare, Introducere, Obiectivele
          Proiectului, Studiu Bibliografic, Analiză şi Fundamentare Teoretică,
          Proiectarea de Detaliu şi Implementare, Testare şi Validare, Manual de
          Instalare şi Utilizare, Concluzii, Bibliografie"
        />
      </p>
      <img src={signup} alt="signup" className="avatar-img" />
      <hr></hr>
      <img src={signup2} alt="signup2" className="avatar-img" />
      <img src={signup3} alt="signup3" className="avatar-img" />
      <p></p>
    </div>
  );
}

export default Home;
