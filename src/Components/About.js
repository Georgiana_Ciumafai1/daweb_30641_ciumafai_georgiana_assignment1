import * as React from "react";
import "../css/About.css";
import { Grid, Cell } from "react-mdl";

function About() {
  return (
    <div style={{ width: "100%", margin: "auto" }}>
      <Grid className="landing-grid">
        <Cell col={2}>
          <div className="banner-text">
            <p>
              <h1>
                <FormattedMessage
                  id="About.titlu"
                  defaultMessage="Sistem expert bazat pe reguli pentru modelarea deciziilor
                                    conducătorilor auto"
                />
              </h1>
            </p>
          </div>
        </Cell>
        <Cell col={2}>
          <div className="banner-text">
            <p>
              <hr />
              <h1>
                <FormattedMessage
                  id="About.obiective"
                  defaultMessage="Obiective"
                />{" "}
              </h1>

              <hr />
            </p>

            <p>
              <FormattedMessage
                id="About.obiective2"
                defaultMessage="Obiectivul general al acestui proiect este reprezintat de către
                implementarea unui framework auxiliar anumitor tehnologii deja
                existente precum Clips, Sumo şi Python, compus dintr-un sistem
                expert ce este dezvoltat folosind drept date de intrare un
                simulator de trafic urban, având capabilitatea de a oferi unui
                eventual utilizator de pildă, student sau participant la trafic,
                instrucţiuni menite să îl ajute în validarea unor decizii. Aceste
                indicaţii rutiere sunt obţinute pe baza anumitor percepţii ce sunt
                generate de către o simulare al unui traseu, cel din urmă dorind
                să fie o reprezentare în mod cât mai realist a unui posibil
                scenariu din viaţa reală."
              />
            </p>
          </div>
        </Cell>
      </Grid>
    </div>
  );
}
export default About;
