import * as React from "react";
import YouTube from "react-youtube";

class News extends React.Component {
  render() {
    const opts = {
      height: "390",
      width: "640",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
      <>
        <div className="banner-text">
          <p>
            <FormattedMessage
              id="News.news1"
              defaultMessage=" SUMO (Simulation of urban mobility) este un program apărut în anul
              2001 dezvoltat de către o echipă de programatori din Germania cu
              scopul de a studia şi de a oferi experienţa unei simulări a
              traficului urban, care să permită imitarea vieţii reale având o
              acurateţe cât mai mare"
            />
          </p>
          <p>
            <FormattedMessage
              id="News.news2"
              defaultMessage=" O altă facilitate foarte importantă ce vine odată cu instalarea
                      acestui program este oferită de către NetConvert, un modul auxiliar
                      SUMO-ului cu ajutorul căruia putem converti hărţi deja existente din
                      diferite surse. NetConvert-ul poate aşadar converti hărţi din
                      diferite surse precum OSM, sau Google Maps pentru a le transpune,
                      astfel încât să poată fi utilizate în cadrul unei simulări. Pe lângă
                      aceasta facilitate, SUMO mai oferă de asemenea şi un alt program
                      intitulat NetEdit, prin intermediul căruia avem posibilitatea fie să
                      creem o hartă de la zero, fie să modificăm una deja existentă, sau
                      să obţinem în mod manual informaţii din ea prin intermediul unei
                      interfeţe grafice."
            />
          </p>
          <p>
            <FormattedMessage
              id="News.news2"
              defaultMessage="  Spre exemplu, pentru a alcătui o rută pentru un autovehicul, NetEdit
                      ne-a permis să identificăm mult mai uşor toate secţiunile de drum de
                      care avem nevoie pentru a alcătui acea rută precum şi intersecţiile
                      întâlnite pe parcus. Pe lângă cele menţionate, tot acest utilitar
                      oferă în momentul apariţiei unor anumite probleme de prioritate în
                      cadrul intersecţiilor nedirijate posibile soluţii prin amplasarea
                      unor semafoare."
            />
          </p>
        </div>
        <div> </div>
        <YouTube videoId="KgPSREMmA_0" opts={opts} onReady={this._onReady} />
      </>
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

export default News;
