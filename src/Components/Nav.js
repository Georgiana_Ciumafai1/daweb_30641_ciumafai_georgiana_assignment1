import * as React from "react";
import { Menubar } from "primereact/menubar";
import "../css/Nav.css";

export class Nav extends React.Component {
  render() {
    return (
      <div className="p-menubar-container">
        <div className="p-menuitem-text">
          <div className="p-menuitem-icon">
            <Menubar
              model={[
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labelhome"
                      defaultMessage="Acasa"
                    />
                  ),
                  icon: "fa fa-home",
                  url: "/home"
                },
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labelnoutati"
                      defaultMessage="Noutati"
                    />
                  ),
                  icon: "fa fa-calendar",
                  url: "/news"
                },
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labeldespre"
                      defaultMessage="Despre Lucrare"
                    />
                  ),
                  icon: "fa fa-file",
                  url: "/about"
                },
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labelstudent"
                      defaultMessage="Profil Student"
                    />
                  ),
                  icon: "fa fa-male",
                  url: "/student"
                },
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labelcoordonator"
                      defaultMessage="Coordonator"
                    />
                  ),
                  icon: "fa fa-male",
                  url: "/coordonator"
                },
                {
                  label: (
                    <FormattedMessage
                      id="Nav.labelcontact"
                      defaultMessage="Contact"
                    />
                  ),
                  icon: "fa fa-phone",
                  url: "/contact"
                }
              ]}
            ></Menubar>
          </div>
        </div>
      </div>
    );
  }
}
export default Nav;
