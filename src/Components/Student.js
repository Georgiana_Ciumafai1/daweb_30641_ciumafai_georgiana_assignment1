import * as React from "react";
import "../css/Student.css";
import signup from "../img/circle-cropped.png";
import { Grid, Cell } from "react-mdl";

function Student() {
  return (
    <div style={{ width: "100%", margin: "auto" }}>
      {" "}
      <Grid className="landing-grid">
        <Cell col={12}>
          <img src={signup} alt="signup" className="avatar-img" />

          <div className="banner-text">
            <h1>Ciumafai Georgiana</h1>

            <hr />

            <p>
              HTML/CSS | Bootstrap | JavaScript | React | CLIPS | Python | Java
              | Spring | Django
            </p>

            <div className="social-links">
              {/* LinkedIn */}
              <a
                href="https://www.linkedin.com/in/georgiana-ciumafai"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="fa fa-linkedin-square" aria-hidden="true" />
              </a>

              {/* Github */}
              <a
                href="https://github.com/GeorgianaCiumafai/Problems"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="fa fa-github-square" aria-hidden="true" />
              </a>

              {/* Bitbucket */}
              <a
                href="https://bitbucket.org/ciumafai_georgiana/profile/repositories"
                rel="noopener noreferrer"
                target="_blank"
              >
                <i className="fa fa-bitbucket" aria-hidden="true" />
              </a>
            </div>
          </div>
        </Cell>
      </Grid>
    </div>
  );
}
export default Student;
